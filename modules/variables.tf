variable "resource_group_location" {
  default     = "eastus"
  description = "Location of the resource group."
}

variable "resource_group_name_prefix" {
  default     = "rg"
  description = "Prefix of the resource group name that's combined with a random ID so name is unique in your Azure subscription."
}

variable "namevm" {
  default = "myVM" 
  description = "myVM"
}

variable "sizevm" {
  default = "Standard_DS1_v2"
  description = "Standard_DS1_v2"
}

variable "disk_name" {
  default = "myOsDisk" 
  description = "myOsDisk"
}

variable "disk_caching" {
  default = "ReadWrite" 
  description = "ReadWrite"
}

variable "disk_storage_account_type" {
  default = "Premium_LRS" 
  description = "Premium_LRS"
}

variable "source_image_reference_publisher" {
  default = "Canonical"
  description = "Canonical"
}

variable "source_image_reference_offer" {
  default = "UbuntuServer"
  description = "UbuntuServer"
}

variable "source_image_reference_sku" {
  default = "18.04-LTS" 
  description = "18.04-LTS"
}

variable "source_image_reference_version" {
  default = "latest" 
  description = "latest"
}

variable "computer_name" {
  default = "myvm" 
  description = "myvm"
}

variable "admin_username" {
  default = "azureuser" 
  description = "azureuser"
}

variable "disable_password_authentication" {
  default = true
  description = "true"
}

variable "admin_ssh_key_username" {
  default = "azureuser"
  description = "azureuser"
}
