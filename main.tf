data "azurerm_resource_group" "name1" {
  name = "rg-grand-dory"
}

data "azurerm_subnet" "azsubnet" {
 name = "mySubnet"
 virtual_network_name = "myVnet"
 resource_group_name = "rg-grand-dory"
}

data "azurerm_public_ip" "pubips" {
  name = "mypublicip"
  resource_group_name = "rg-grand-dory"
}

data "azurerm_storage_account" "storacc" {
  name                = "azstaccountname"
  resource_group_name = "rg-grand-dory"
}

data "azurerm_network_security_group" "aznetwork" {
  name                = "myNetworkSecurityGroup"
  resource_group_name = "rg-grand-dory"
}

resource "azurerm_linux_virtual_machine" "my_terraform_vm" {
  name                  = var.namevm
  location              = "eastus"
  resource_group_name   = data.azurerm_resource_group.name1.name
  network_interface_ids = [azurerm_network_interface.my_terraform_nic.id]
  size                  = var.sizevm

  os_disk {
    name                 = var.disk_name
    caching              = var.disk_caching
    storage_account_type = var.disk_storage_account_type
  }

  source_image_reference {
    publisher = var.source_image_reference_publisher
    offer     = var.source_image_reference_offer
    sku       = var.source_image_reference_sku
    version   = var.source_image_reference_version
  }

  computer_name                   = var.computer_name
  admin_username                  = var.admin_username
  disable_password_authentication = var.disable_password_authentication

  admin_ssh_key {
    username   = var.admin_ssh_key_username
    public_key = tls_private_key.example_ssh.public_key_openssh
  }

  boot_diagnostics {
    storage_account_uri = data.azurerm_storage_account.storacc.primary_blob_endpoint
  }
}


resource "azurerm_network_interface" "my_terraform_nic" {
  name                = "myNIC"
  location            = data.azurerm_resource_group.name1.location
  resource_group_name = data.azurerm_resource_group.name1.name

  ip_configuration {
    name                          = "my_nic_configuration"
    subnet_id                     = data.azurerm_subnet.azsubnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = data.azurerm_public_ip.pubips.id
  }
}

resource "azurerm_network_interface_security_group_association" "example" {
  network_interface_id      = azurerm_network_interface.my_terraform_nic.id
  network_security_group_id = data.azurerm_network_security_group.aznetwork.id
}

resource "tls_private_key" "example_ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
